#  Image\_classification_deep_learning

This repository is used to train a deep neural network image classifier and test it on a specific image. 

>**How to Use**:
>  * Install the necessary python dependencies (found in pyproject.toml) with "pip install -e ." preferably in a venv or conda environment.  
>  * The needed configuration files needed to for this repository are located at src/config. Update them when needed.
>  * Use train.py to train an image classifier.
>  * Use predict.py to classify a specific image.

